(load "./src/minimal_compiler/runtime/consts.scm")
(load "./src/minimal_compiler/runtime/var.scm")

(define make-symbol-hashtable (lambda [] (make-hashtable symbol-hash eq?)))

(define namespaces (make-symbol-hashtable))

(define-record-type namespace
    (fields name mapping aliases)
    (protocol 
        (lambda [new] 
            (lambda [sym] (assert (symbol? sym))(new sym (make-symbol-hashtable) (void))))))

(define find-ns
    (lambda [sym]
        (symbol-hashtable-ref namespaces sym nil)))


(define create-ns 
    (lambda [sym] 
        (let ([cell (symbol-hashtable-cell namespaces sym nil)])
            (if (null? (cdr cell))
                (let ([new-ns (make-namespace sym)]) 
                    (set-cdr! cell new-ns)
                    new-ns)
                (cdr cell)))))

(define core (create-ns 'clojure.core))
(define *ns* (make-unbound-var core '*ns*))

(define remove-ns 
    (lambda [sym] 
        (let ([cell (symbol-hashtable-cell namespaces sym nil)])
            (if (null? (cdr cell))
                nil
                (begin 
                    (symbol-hashtable-delete! namespaces sym)
                    (cdr cell))))))

(define all-ns (lambda [] (hashtable-values namespaces)))

(define the-ns 
    (lambda [x] 
        (if (namespace? x)
            x
            (let ([ns (find-ns x)]) 
                (if (null? ns) 
                    (raise (string-append "Non namespace: " (symbol->string x) " found"))
                    ns)))))

(define ns-name 
    (lambda [ns]
        (symbol->string (namespace-name (the-ns ns)))))

(define ns-map
    (lambda [ns]
        (hashtable-cells (namespace-mapping (the-ns ns)))))

(define ns-unmap
    (lambda [ns sym]
        (symbol-hashtable-delete! (namespace-mapping (the-ns ns)) sym) nil))

(define ns-resolve
    (lambda [ns sym]
        (cdr (symbol-hashtable-cell (namespace-mapping (the-ns ns)) sym nil))))

(define resolve
    (lambda [sym]
        (ns-resolve (var-get *ns*) sym)))

(define intern
    (lambda [ns sym value]
        (let ([cell (symbol-hashtable-cell (namespace-mapping (the-ns ns)) sym nil)])
            (set-cdr! cell (make-var ns sym value))
            (cdr cell))))

(define intern-var
    (lambda [ns sym var]
        (let ([cell (symbol-hashtable-cell (namespace-mapping (the-ns ns)) sym nil)])
            (set-cdr! cell var)
            (cdr cell))))



(intern-var core '*ns* *ns*)

; TODO
; (define namespace/intern) (namespace-intern ?)
; cf https://github.com/clojure/clojure/blob/da0b9574017918deede6d2a15f386a7cc1b70a2c/src/jvm/clojure/lang/Namespace.java#L50