# Minimal Compiler

Here, we are defining a minimal set of structures in order to compile a very restricted subset of Clojure. The goal, here, is to get an idea of how the toolchain should work, and what the runtime library should implement.

Right now, it compiles theses nodes :
* def
* fn
* fn-method
* do
* var
* local
* if
* const
* invoke

We still want to add :
* let
* bindings

The `fn` nodes contains `fn-method` nodes which correspond to each subfunction of a Clojure's function.
The `var` targets namespace variable and the `local` targets local variables



