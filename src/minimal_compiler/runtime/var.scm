;; singleton ?
(define-record-type unbound)

(define-record-type var
    (fields (mutable root) dynamic sym ns)
    (protocol 
        (lambda [new]
            (lambda [ns sym root]
                (new root #f sym ns)))))



(define deref var-root)
(define var-get deref)

(define make-unbound-var
    (lambda [ns sym]
        (make-var ns sym (make-unbound))))

(define bound?
    (lambda [var]
        (not (unbound? (var-get var)))))

;; there is no '!' in clojure's var-set
(define var-set var-root-set!)

; We need 'symbol/intern' first (symbol parsing)
; (define find-var)

