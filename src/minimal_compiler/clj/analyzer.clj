(ns minimal-compiler.clj.analyzer
  (:refer-clojure :exclude [macroexpand-1 macroexpand ns])
  (:require
   

   [minimal-compiler.clj.parser :refer [parse specials not-inlined ignored]]

   [clojure.tools.analyzer
    :as ana
    :refer [analyze analyze-in-env wrapping-meta analyze-fn-method]
    :rename {analyze -analyze}]

   [clojure.tools.analyzer
    [utils :refer [ctx resolve-sym -source-info resolve-ns obj? dissoc-env butlast+last mmerge]]
    [ast :refer [walk prewalk postwalk] :as ast]
    [env :as env :refer [*env*]]
    [passes :refer [schedule]]
    [jvm :as jvm]]

   [clojure.tools.analyzer.jvm.utils :as jvm-utils]

   [minimal-compiler.clj.passes
    [emit-form :refer [emit-form]]]

   [clojure.tools.analyzer.passes
    [source-info :refer [source-info]]
    [trim :refer [trim]]
    [elide-meta :refer [elide-meta elides]]
    [warn-earmuff :refer [warn-earmuff]]
    [uniquify :refer [uniquify-locals]]]))

(def default-passes
  "Set of passes that will be run by default on the AST by #'run-passes"
  #{;; #'warn-on-reflection
    #'warn-earmuff

    #'uniquify-locals

    #'source-info
    #'elide-meta
    ;; #'constant-lift

    #'trim

    ;; #'box

    ;; to defined
    ;; #'analyze-host-expr

    ;; #'validate-loop-locals

    ;; to defined

    ;; #'validate

    ;; to defined
    ;; #'infer-tag

    ;; #'classify-invoke
    })

(def scheduled-default-passes
  (schedule default-passes))

(defn ^:dynamic run-passes
  "Function that will be invoked on the AST tree immediately after it has been constructed,
   by default runs the passes declared in #'default-passes, should be rebound if a different
   set of passes is required.

   Use #'clojure.tools.analyzer.passes/schedule to get a function from a set of passes that
   run-passes can be bound to."
  [ast]
  (scheduled-default-passes ast))


(defn macroexpand-1
  "If form represents a macro form returns its expansion, else returns form."
  [form env]
  (if (seq? form)
    (let [op (first form)]
      (if (or (not (symbol? op))
              (specials op)
              (not-inlined op)
              (ignored op))
        form
        (jvm/macroexpand-1 form env)))
    (jvm/macroexpand-1 form env)))


(defn analyze
  "Analyzes a clojure form using tools.analyzer augmented with the JVM specific special ops
   and returns its AST, after running #'run-passes on it.

   If no configuration option is provides, analyze will setup tools.analyzer using the extension
   points declared in this namespace.

   If provided, opts should be a map of options to analyze, currently the only valid
   options are :bindings and :passes-opts (if not provided, :passes-opts defaults to the
   value of `default-passes-opts`).
   If provided, :bindings should be a map of Var->value pairs that will be merged into the
   default bindings for tools.analyzer, useful to provide custom extension points.
   If provided, :passes-opts should be a map of pass-name-kw->pass-config-map pairs that
   can be used to configure the behaviour of each pass.

   E.g.
   (analyze form env {:bindings  {#'ana/macroexpand-1 my-mexpand-1}})"
  ([form] (analyze form (jvm/empty-env) {}))
  ([form env] (analyze form env {}))
  ([form env opts]
   (with-bindings (merge {;; Compiler/LOADER     (RT/makeClassLoader)
                          #'ana/macroexpand-1 macroexpand-1
                          #'ana/create-var    jvm/create-var
                          #'ana/parse         parse
                          #'ana/var?          var?
                          ;; #'elides            (merge {:fn    #{:line :column :end-line :end-column :file :source}
                          ;;                             :reify #{:line :column :end-line :end-column :file :source}}
                          ;;                            elides)
                          #'*ns*              (the-ns (:ns env))}
                         (:bindings opts))
     (env/ensure (jvm/global-env)
                 (let [ast (env/with-env (mmerge (env/deref-env)
                                                 {:passes-opts (get opts :passes-opts jvm/default-passes-opts)})
                             (run-passes (-analyze form env)))]
                   (jvm/update-ns-map!)
                   ast)))))

(deftype ExceptionThrown [e ast])

(defn ^:private throw! [e]
  (throw (.e ^ExceptionThrown e)))

(defn analyze+eval
  "Like analyze but evals the form after the analysis and attaches the
   returned value in the :result field of the AST node.

   If evaluating the form will cause an exception to be thrown, the exception
   will be caught and wrapped in an ExceptionThrown object, containing the
   exception in the `e` field and the AST in the `ast` field.

   The ExceptionThrown object is then passed to `handle-evaluation-exception`,
   which by defaults throws the original exception, but can be used to provide
   a replacement return value for the evaluation of the AST.

   Unrolls `do` forms to handle the Gilardi scenario.

   Useful when analyzing whole files/namespaces."
  ([form] (analyze+eval form (jvm/empty-env) {}))
  ([form env] (analyze+eval form env {}))
  ([form env {:keys [handle-evaluation-exception]
              :or {handle-evaluation-exception throw!}
              :as opts}]
   (env/ensure (jvm/global-env)
               (jvm/update-ns-map!)
               (let [env (merge env (-source-info form env))
                     [mform raw-forms] (with-bindings {;;  Compiler/LOADER     (RT/makeClassLoader)
                                                       #'*ns*              (the-ns (:ns env))
                                                       #'ana/macroexpand-1 (get-in opts [:bindings #'ana/macroexpand-1] macroexpand-1)}
                                         (loop [form form raw-forms []]
                                           (let [mform (ana/macroexpand-1 form env)]
                                             (if (= mform form)
                                               [mform (seq raw-forms)]
                                               (recur mform (conj raw-forms
                                                                  (if-let [[op & r] (and (seq? form) form)]
                                                                    (if (or (jvm-utils/macro? op  env)
                                                                            (jvm-utils/inline? op r env))
                                                                      (vary-meta form assoc ::ana/resolved-op (resolve-sym op env))
                                                                      form)
                                                                    form)))))))]
                 (if (and (seq? mform) (= 'do (first mform)) (next mform))
           ;; handle the Gilardi scenario
                   (let [[statements ret] (butlast+last (rest mform))
                         statements-expr (mapv (fn [s] (analyze+eval s (-> env
                                                                           (ctx :ctx/statement)
                                                                           (assoc :ns (ns-name *ns*)))
                                                                     opts))
                                               statements)
                         ret-expr (analyze+eval ret (assoc env :ns (ns-name *ns*)) opts)]
                     {:op         :do
                      :top-level  true
                      :form       mform
                      :statements statements-expr
                      :ret        ret-expr
                      :children   [:statements :ret]
                      :env        env
                      :result     (:result ret-expr)
                      :raw-forms  raw-forms})
                   (let [a (analyze mform env opts)
                         frm (emit-form a)
                         result (try (eval frm) ;; eval the emitted form rather than directly the form to avoid double macroexpansion
                                     (catch Exception e
                                       (handle-evaluation-exception (ExceptionThrown. e a))))]
                     (merge a {:result    result
                               :raw-forms raw-forms})))))))

(defn select-ast
  ([ast accepted] (select-ast ast accepted #{}))
  ([ast accepted rejected]
   (postwalk ast #(select-keys % (apply disj (into (into #{:children :op} (:children %)) accepted) rejected)))))