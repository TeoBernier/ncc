(ns minimal-compiler.clj.compiler
  (:require
   [clojure.string :as string]
   [minimal-compiler.clj.analyser :as min-ana]
   [clojure.tools.analyzer.ast :refer [postwalk children] :as ast]
   [clojure.set :as set]))




(defn pipe-ize
  [sym]
  (symbol (str "|" sym "|")))

(def ns-symbols #{'find-ns 'create-ns 'remove-ns 'all-ns 'the-ns 'ns-name 'ns-map 'ns-unmap 'ns-resolve 'resolve 'intern})

(defn var-to-name [var]
  (let [meta-var (meta var)
        name (:name meta-var)
        ns-sym (ns-name (:ns meta-var))]
    (pipe-ize (symbol (str ns-sym "/" name)))))


(defn get-defines [ast]
  (postwalk
   ast
   (fn [{:keys [op] :as node}]
     (if (= op :def)
       (conj (apply set/union (children node)) (var-to-name (:var node)))
       (apply set/union (children node))))))

(defn statically-compilable [ast]
  (postwalk
   ast
   (fn [{:keys [op] :as node}]
     (and (not (and (= op :var)
                    (let [meta-var (meta (:var node))
                          name (:name meta-var)
                          ns-sym (ns-name (:ns meta-var))]
                      (and (= ns-sym 'clojure.core)
                           (contains? ns-symbols name)))))
          (every? identity (children node))))))

(defmulti -emit-scheme
  (fn [{:keys [op]} _] op))

(defn emit-scheme
  ([ast] (-emit-scheme ast #{}))
  ([ast & opts] (-emit-scheme ast (into #{} opts))))


(defmethod -emit-scheme :const
  [{:keys [form]} _]
  form)

(defmethod -emit-scheme :ns
  [{:keys [name]} opts]
  (if (contains? opts :static)
    ;; While compiling statically, we want all namespaces to disappear at runtime
    ;; hence, we do not want to compile the ns part of the program.
    ;; It will probably have to change for separate compilation.
    nil

    ;; Switch / namespace setup
    (list 'in-ns (list 'quote (pipe-ize name)))))

;; (defmethod -emit-scheme :var
;;   [{:keys [form]} _]
;;   `(~'get-from-current-namespace ~form))

(defmethod -emit-scheme :var
  [{:keys [var]} opts]
  (let [meta-var (meta var)
        name (:name meta-var)
        ns-sym (ns-name (:ns meta-var))]

    (if (contains? opts :static)
      ;; To compile statically, we rename variable by adding their namespace in their name.
      (var-to-name var)

      ;; To look for a variable, we first have to find the namespace it corresponds to.
      (list 'var-get (list 'ns-resolve (list 'quote ns-sym) (list 'quote (pipe-ize name)))))))

;; (defmethod -emit-scheme :def
;;   [{:keys [name init]} opts]
;;   `(~'push-to-current-namespace ~name ~(if (nil? init) '(unboud-value) (-emit-scheme init opts))))

(defmethod -emit-scheme :def
  [{:keys [name init var]} opts]
  (if (contains? opts :static)
    ;; To handle a def statically, we could simply choose to use define
    ;; but it isn't really a choice since def in clojure are way more permissive
    ;; than define in scheme. Hence, we handle every def the same way, using set!.
    (when init (list 'set! (var-to-name var) (-emit-scheme init opts)))


    (list 'intern '(var-get *ns*) (list 'quote (pipe-ize name)) (if (nil? init) '(make-unbound) (-emit-scheme init opts)))))

(defmethod -emit-scheme :fn
  [{:keys [methods variadic?]} opts]
  (let [methods (sort-by #(+ (:fixed-arity %) (if (:variadic? %) 1 0)) methods)
        fixed-arity-methods (if variadic? (butlast methods) methods)
        fixed-arity-bindings (map #(vector (symbol (str "____f" (:fixed-arity %))) (-emit-scheme % opts)) fixed-arity-methods)
        variadic-method (when variadic? (last methods))
        variadic-binding (when variadic? (vector (symbol "____v") (-emit-scheme variadic-method opts)))
        bindings (if variadic? (cons variadic-binding fixed-arity-bindings) fixed-arity-bindings)
        error-arity '((else (arity-error ____len)))]

    (list 'let bindings
          (list 'lambda 'args (list 'let (list (vector '____len '(length args)))
                                    (concat (cons 'cond (map #(list (list '= '____len (:fixed-arity %))
                                                                    (list 'apply (symbol (str "____f" (:fixed-arity %))) 'args)) fixed-arity-methods))
                                            (if variadic? (cons (list (list '<= (:fixed-arity variadic-method) '____len) (list 'apply (symbol "____v") 'args)) error-arity) error-arity)))))))

(defmethod -emit-scheme :fn-method
  [{:keys [variadic? params body]} opts]
  (let [std-params (if variadic? (butlast params) params)
        v-param (when variadic? (last params))]
    `(~'lambda ~(if (and variadic? (= 1 (count params))) (pipe-ize (:name v-param)) (into (into [] (map #(pipe-ize (:name %)) std-params)) (when variadic? `(. ~(pipe-ize (:name v-param)))))) ~(-emit-scheme body opts))))

(defmethod -emit-scheme :do
  [{:keys [ret statements body?]} opts]
  (if (and body? (empty? statements))
    (-emit-scheme ret opts)
    `(~'begin
      ~@(map #(-emit-scheme % opts) statements)
      ~(-emit-scheme ret opts))))

; nil in scheme ? unhandle case
(defmethod -emit-scheme :if
  [{:keys [test then else]} opts]
  `(if ~(-emit-scheme test opts)
     ~(-emit-scheme then opts)
     ~@(when-not (nil? (:form else))
         [(-emit-scheme else opts)])))


(defmethod -emit-scheme :invoke
  [{:keys [fn args]} opts]
  (let [expr `(~(-emit-scheme fn opts)
               ~@(map #(-emit-scheme % opts) args))]
    expr))

(defmethod -emit-scheme :maybe-class
  [{:keys [form]} opts]
  form)

(defmethod -emit-scheme :with-meta
  [{:keys [expr]} opts]
  (-emit-scheme expr opts))


(defmethod -emit-scheme :local
  [{:keys [name]} _]
  (pipe-ize name))

(defmethod -emit-scheme :quote
  [{:keys [expr]} opts]
  (list 'quote (-emit-scheme expr opts)))

(defmethod -emit-scheme :ignore
  [{:keys [expr]} opts]
  'nil)



(defn compile-form [form]
  (let [current-ns *ns*
        analyzed-form (min-ana/analyze+eval form)
        compiled-expr (if (statically-compilable analyzed-form)
                        ;; We try to compile statically
                        (let [defines (get-defines analyzed-form)]
                          (list*
                           'begin
                           '(load "./src/minimal_compiler/runtime/static_core.scm")
                           (seq (conj
                                 (mapv #(list 'define %) defines)
                                 (emit-scheme analyzed-form :static)))))

                        ;; but if it fails, we compile with namespaces
                        (list
                         'begin
                         '(load "./src/minimal_compiler/runtime/core.scm")
                         (emit-scheme analyzed-form)))]
    (set! *ns* current-ns)
    compiled-expr))


(defn compile-forms
  [& forms]
  (compile-form (cons 'do forms)))

(defmacro compile-prog
  [& forms]
  (list 'quote (apply compile-forms forms)))