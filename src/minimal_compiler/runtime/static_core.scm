;; start scheme session with (load "./src/minimal_compiler/runtime/core.scm")
(load "./src/minimal_compiler/runtime/consts.scm")
(load "./src/minimal_compiler/runtime/var.scm")
(load "./src/minimal_compiler/runtime/primitives.scm")

;; numbers methods
(define |clojure.core/+| +)
(define |clojure.core/-| -)
(define |clojure.core/*| *)
(define |clojure.core//| /)
(define |clojure.core/mod| mod)
(define |clojure.core/<| <)
(define |clojure.core/<=| <=)
(define |clojure.core/>| >)
(define |clojure.core/>=| >=)

(define |clojure.core/dec| (lambda [x] (- x 1)))
(define |clojure.core/inc| (lambda [x] (+ x 1)))

(define |clojure.core/zero?| zero?)


;; numbes primitives
(define inc (lambda [n] (+ n 1)))
(define dec (lambda [n] (- n 1)))


;; general functions
(define |clojure.core/apply| apply)
(define |clojure.core/print| display)
(define |clojure.core/println| (lambda [x] (display x) (newline)))


;; exactly the same since '=' is type independent
(define |clojure.core/=| equal?)

;;| var methods
(define |clojure.core/var-get| var-get)
(define |clojure.core/var-set| var-set)
(define |clojure.core/var?| var?)
(define |clojure.core/bound?| bound?)

;; list methods
;; not exactly the same since clojure does not support pair, but only list
(define |clojure.core/cons| cons)
(define |clojure.core/list| list)
