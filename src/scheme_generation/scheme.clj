(ns scheme-generation.scheme)

(defn quoted-pairs
  [elems]
  (loop [[e1 e2 & elems] elems acc []]
    (if (seq? elems)
      (recur elems (into acc [[(list 'quote (list 'quote e1)) e2]]))
      (into acc [[(list 'quote (list 'quote e1)) e2]]))))

(defn scheme-begin
  [& seq]
  (list* 'list ''begin seq))

(defn scheme-define
  ([sym]
   (list 'list ''define (list 'quote sym)))
  ([sym body]
   (list 'list ''define (list 'quote sym) body)))

(defn scheme-expr
  [expr]
  (list 'quote expr))

(defn scheme-quoted-expr
  [clj-expr]
  (list 'quote (list 'quote clj-expr)))

(defn scheme-parentheses
  [& elems]
  (list* 'list elems))

(defn scheme-list
  [& elems]
  (list* 'list ''list elems))

(defn scheme-display
  [& elems]
    (list 'list ''display (if (= 1 (count elems)) (first elems) (apply scheme-list elems))))

(defn scheme-newline
  []
  (list 'list ''newline))

(defn scheme-displayln
  [& elems]
  (scheme-begin (apply scheme-display elems) (scheme-newline)))



(defmacro mscheme-let
  [[& bindings] & seq]
  (list* 'list ''list '''let (cons 'list (cons ''list (quoted-pairs bindings))) seq))

(defmacro mscheme-can-expr
  [sym]
  (list 'list ''quote (list 'quote sym)))

(defmacro mscheme-invoke
  [sym & elems]
  (list* 'list ''list (list 'quote (list 'quote sym)) elems))

(defmacro mscheme-define
  ([sym]
   (list 'list ''list '''define (list 'quote (list 'quote sym))))
  ([sym body]
   (list 'list ''list '''define (list 'quote (list 'quote sym)) body)))